#!/usr/bin/env bash
ssh -o StrictHostKeyChecking=no root@47.47.47.47 << 'ENDSSH'
 cd /testdjango
 docker login -u $REGISTRY_USER -p $CI_BUILD_TOKEN $CI_REGISTRY
 docker pull registry.gitlab.com/v.g.galkin/testdjango:latest
 docker-compose up -d
ENDSSH
